# phib

Phib aims to offer the absolute simplest way of working with bibliographies on the command line.

Phib lets you:

- `countbib`: Group and count references by keyword
- `viewbib`: Generate HTML versions of your BibTeX (via `bibtex2html`)
- `doi`: Query the [Crossref](https://api.crossref.org) API to find bibliography information

## Setup

### Requirements

Phib primarily uses standard UNIX utils which should not require installation:

- `awk`
- `sed`
- `tr`
- `sort`

#### Additional 

For generating an HTML version of your `.bib` file, `[bibtex2html](https://www.lri.fr/~filliatr/bibtex2html/)` is required.

OpenBSD:

```
doas pkg_add bibtex2html
```

Ubuntu:

```
sudo apt install bibtex2html
```

For querying the Crossref API, `curl` is required:

OpenBSD:

```
doas pkg_add curl
```

Ubuntu:

```
sudo apt install curl
```

### Configuration

It is reccomended that Phib be used via environment variables.

To do so, add the following to your `.bashrc` or `.profile`:

```
export PHIB_BIB="$HOME/bib.bib" # Your BibTeX file
export PHIB_EDITOR=vim # Preferred editor
export PHIB_TITLE='Research Project' # Title used when generating HTML version of BibTeX
export PHIB_OUT=/tmp/bib # Output directory for HTML version (Omit .html extension)
```

### Using `doi`

By default, `doi` will print to standard out. This allows for easy
integration with additional commands and scripts however the easiest use
case is to append the API response to your bibliography:

```
doi 10.1145/3476058 >> $BIB
```

**NOTE:** Always use `>>` when appending to your bibliography. Using `>` will overwrite the contents of your bibliography.
